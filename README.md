# AcouMan - a platform for acoustic manipulation
[index](README.md)

AcouMan is a platform for acoustic manipulation, that is for manipulating objects via shaping the surrounding acoustic pressure field. You can see the platform in the figure below and a detailed description will be (hopefully) published soon. If you want to see the platform in action, check our [video](https://youtu.be/BdDq_jRSNYA) showing some experiments with the platform.

This repository contains all files needed to reproduce the platform.

* [_matlab/_](matlab/readme.md) folder contains Matlab scripts implementing a BFGS solver, calibration, GUIs and visualization
* [_FPGA-generator/_](FPGA-generator/readme.md) folder contains a Quartus project for the 64 channel generator
* _manufacturing/_ folder contains all drawings for a [laser cutter](manufacturing/lasercut/readme.md), [3D models](manufacturing/3Dprint/readme.md) and [PCB designs](manufacturing/PCB/readme.md) for manufacturing the platform
* [_simulink/_](simulink/readme.md) folder contains Simulink schemes for controlling the platform in various modes
* [_webinterface/_](webinterface/readme.md) folder contains Python scripts for the web interface running on the Raspberry Pi

## Building the platform

If you want to build the platform, please refer to the guides on [mechanical assembly](docs/assembly.md) and [software](docs/software.md).

![Photos of the platform](docs/platfromCompo_annotateed.png)
